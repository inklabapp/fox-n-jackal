using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlayerCharacter : MonoBehaviour
{
    // Start is called before the first frame update
    private bool isOnGround;
    private bool jumpAvailable;
    private float faceDir = -1;
    private float neonSpawnDist = 3.0f;
    public GameObject arcObject;
    public GameObject neonObject;
    public float walkForce = 50.0f;
    public float jumpForce = 900.0f;
    private int selectedNeonIdx = 0;

    public GameObject[] neonShapes;

     private Vector3 validDirection = Vector3.up;  // What you consider to be upwards
    private float contactThreshold = 70;          // Acceptable difference in degrees

    void Start()
    {
        isOnGround = false;
        jumpAvailable = true;
    }

    void drawPointer(){
        
        Vector3 unknownoffset = new Vector3(0.63f, -1.12f, 0);
        Vector3 focusObject = GetComponent<Renderer>().bounds.center;
        Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mousedirrel = new Vector3(mouse.x - focusObject.x, mouse.y - focusObject.y, 0) + unknownoffset; 
        //Debug.Log(mousedirrel);
        Ray ray = new Ray(focusObject, mousedirrel);        
        arcObject.transform.position = ray.GetPoint(3.0f);
        

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject obj = Instantiate(neonObject, arcObject.transform.position, Quaternion.identity) as GameObject;
        }

        

        //drawPointer();
        if (Input.GetAxisRaw("Vertical") > 0)
        {
            //Vector3 forceVector = new Vector3(0, 0, 5.0f);
            //GetComponent<Rigidbody>().AddRelativeForce(forceVector);    
            if (isOnGround && jumpAvailable){
                
                Vector3 forceVector = new Vector3(0, jumpForce, 0);
                GetComponent<Rigidbody>().AddRelativeForce(forceVector);
                jumpAvailable = false;    
            }            
        }
        if (Input.GetAxisRaw("Vertical") == 0)
        {
            if (isOnGround){
                jumpAvailable = true;    
            }            
        }
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            faceDir = 3;
            Vector3 forceVector = new Vector3(walkForce, 0, 0);
            GetComponent<Rigidbody>().AddRelativeForce(forceVector);
        
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            faceDir = -1;
            Vector3 forceVector = new Vector3(-walkForce, 0, 0);
            GetComponent<Rigidbody>().AddRelativeForce(forceVector);
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump")){
            if (neonShapes.Length > 0){
                Vector3 spawnPos = new Vector3(neonSpawnDist * faceDir, 1, 0) + gameObject.transform.position;
                GameObject obj = Instantiate(neonShapes[selectedNeonIdx], spawnPos, Quaternion.identity) as GameObject;    
            }            
        }
    }

    void OnCollisionEnter(Collision col)
    {
    
        Collider collider = col.collider;
        if (collider.CompareTag("ground"))
        {
            //isOnGround = true;
            for (int k=0; k < col.contacts.Length; k++) 
            {
                if (Vector3.Angle(col.contacts[k].normal, validDirection) <= contactThreshold)
                {
                    // Collided with a surface facing mostly upwards
                    isOnGround = true;
                    break;
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
    // the Collision contains a lot of info, but it’s the colliding
    // object we’re most interested in.
        Collider collider = collision.collider;
        if (collider.CompareTag("ground"))
        {
            isOnGround = false;
        }
    }

}
